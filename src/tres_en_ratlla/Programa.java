package tres_en_ratlla;

import java.util.Scanner;

public class Programa {

	static Scanner src = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int opcio;
		String jugador1 = "";
		String jugador2 = "";
		boolean definido = false;
		String ganador;

		do {
			opcio = menu();
			switch (opcio) {
			case 1:
				Ajuda.MostrarAjuda();
				break;
			case 2:
				jugador1 = Jugador.DefinirJugador();
				jugador2 = Jugador.DefinirJugador();
				definido = true;
				break;
			case 3:
				if (definido) {
					ganador = Jugar.jugarPartida(jugador1, jugador2);
					actualitzarResultats(ganador, jugador1, jugador2);
					definido = false;
				} else
					System.out.println("Error, cal definir primer el jugador");
				break;
			case 4:
				Jugador.mostrarJugador(jugador1);
				Jugador.mostrarJugador(jugador2);
				break;
			case 5:
				Jugador.mostrarJugadores();
				break;
			case 0:
				System.out.println("GG\n\nBye!!!");
				break;
			default:
				System.out.println("Error, opcio incorrecta. Torna-ho a provar..");
			}

		} while (opcio != 0);
	}

	private static void actualitzarResultats(String ganador, String jugador1, String jugador2) {
		// TODO Auto-generated method stub
		if (ganador != null) {
			if (ganador.equals(jugador1)) {
				Jugador.actualizar(jugador1, "gana");
				Jugador.actualizar(jugador2, "pierde");
			} else {
				Jugador.actualizar(jugador2, "gana");
				Jugador.actualizar(jugador1, "pierde");
			}
		} else {
			Jugador.actualizar(jugador2, "pierde");
			Jugador.actualizar(jugador1, "pierde");
		}
	}

	private static int menu() {
		// TODO Auto-generated method stub
		System.out.println("1.- Mostrar Ajuda");
		System.out.println("2.- Definir Jugador");
		System.out.println("3.- Jugar Partida");
		System.out.println("4.- Veure Jugador");
		System.out.println("5.  Ranking jugadores");
		System.out.println("0.- Sortir");
		System.out.println("\nEscull una opcio: ");

		return leerEntero();

	}

	private static int leerEntero() {
		// TODO Auto-generated method stub
		int op = 0;
		boolean valid = false;

		do {
			try {
				op = src.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.println("Error, has de posar un numero enter");
				src.nextLine();
			}
		} while (!valid);

		return op;
	}

}

package tres_en_ratlla;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Jugar {

	static Scanner src = new Scanner(System.in);

	public static String jugarPartida(String j1, String j2) {
		// Aquest mètode torna el nom del jugador que ha guanyat. Si no guanya ningú,
		// retorna NULL
		// Declaració de variables
		final char[] fitxa = { 'X', 'O' };
		final int MAX = 3;
		char[][] tauler; // tauler de joc
		boolean guanya; // indica si s'ha guanyat la partida
		int casellesOcupades; // compta les caselles ocupades
		int torn; // valors: 1 o 2: 1, jugador1, 2: jugador2
		String jugadorGuanyador; // guarda el nom del jugador que ha guanyat. Si no guanya ningú, serà NULL

		// Inicialització de la partida - Preparació del joc
		tauler = inipartida(MAX);
		guanya = false;
		casellesOcupades = 0;
		torn = 0;
		// Desenvolupament del joc
		while (guanya == false && casellesOcupades < MAX * MAX) {
			mostrarEstat(tauler, torn);
			tiradaJugador(tauler, fitxa, torn);
			casellesOcupades++;
			guanya = comprova(tauler);
			torn = (torn + 1) % 2; // comptador circular: 0,1,0,1....
			// torn = (torn % 2) + 1; //comptador circular: 1,2,1, 2....
		}
		jugadorGuanyador = mostraResultats(tauler, torn, guanya, j1, j2);

		return jugadorGuanyador; // retorna el nom del jugador que ha guanyat la partida. Si la partida acaba en
									// taules, retorna NULL
	}

	private static String mostraResultats(char[][] tauler, int torn, boolean guanya, String j1, String j2) {
		// TODO Auto-generated method stub
		return null;
	}

	private static boolean comprova(char[][] tauler) {
		// TODO Auto-generated method stub
		return false;
	}

	private static void tiradaJugador(char[][] tauler, char[] fitxa, int torn) {
		// TODO Auto-generated method stub

	}

	private static void mostrarEstat(char[][] tauler, int torn) {
		// TODO Auto-generated method stub

	}

	private static char[][] inipartida(int mAX) {
		// TODO Auto-generated method stub
		return null;
	}

}

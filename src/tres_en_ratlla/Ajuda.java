package tres_en_ratlla;

public class Ajuda {

	public static void MostrarAjuda() {
		// TODO Auto-generated method stub
		System.out.println("****  Instruccions joc 3 en Ratlla  ******");
		System.out.println("----------------------------------------------");
		System.out.println("En aquest joc participen dos jugadors representats al tauler de joc 3x3 per O y X.\n");
		System.out.println("Cada jugador, al seu torn, marca amb el seu simbol la casella que vol ocupar.\n");
		System.out.println(
				"Un jugador guanya si aconsegueix tenir una linia de tres dels seus simbols: la l�nia pot ser horitzontal, vertical o diagonal. \n");
		System.out.println("Mucha suerte y a jugar....\n");

		System.out.println("-----------------------------------------------");

	}

}

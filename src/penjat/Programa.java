package penjat;

import penjat.Ajuda;
import penjat.Jugador;
import penjat.Partida;
import java.util.Scanner;

public class Programa {

	static Scanner src = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int opcio;
		String jugador = "";
		boolean reg = false;
		do {

			opcio = mostrarMenu(); // mètode definit per nosaltres. Retorna l'opció de menú escollida per l'usuari

			switch (opcio) {
			case '1':
				Ajuda.MostrarAjuda();
				break;
			case '2':
				jugador = Jugador.DefinirJugador();
				reg = true;
				break;
			case '3':
				if (!reg) {
					Partida.JugarPartida(jugador);
				} else
					System.out.println("\nNo es correcto");
				break;
			case '4':
				Jugador.VeureJugador(jugador);
				break;
			case '5':
				Jugador.Inicialitzar();
				reg = true;
			case '6':
				Jugador.MostrarEstadistica();
			case '0':
				System.out.println("\nNice game");
				break;
			default:
				System.out.println("Error, opcio incorrecta. Torna-ho a provar..");
			}
		} while (opcio != '0');
		System.out.print("\n‎ 😃 !!!");
	}

	private static int mostrarMenu() {
		// TODO Auto-generated method stub
		int op = 0;
		boolean valid = false;

			System.out.println("\nEl Penjat:\n");
			System.out.println("1.- Mostrar ajuda:");
			System.out.println("2.- Definir jugador:");
			System.out.println("3.- Jugar partida:");
			System.out.println("4.- Veure jugador:");
			System.out.println("5.- Inicialitzar partida:");
			System.out.println("6.- Mostrar Estadistica:");
			System.out.println("0. Sortir");
			System.out.print("\n Tria una opció: ");
			
			do {
				try {
					op = src.nextInt();
					valid = true;
				}
				catch (Exception e) {
					System.out.println("Error, has de posar un n�mero enter");
					src.nextLine();
				}
			} while(!valid);
			
			return op;
		}
	}
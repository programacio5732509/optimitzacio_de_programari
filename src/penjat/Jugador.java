package penjat;

import java.util.ArrayList;
import java.util.Scanner;

public class Jugador {

	static ArrayList<String> noms = new ArrayList<String>();
	static ArrayList<Integer> partidesGuanyades = new ArrayList<Integer>();
	static ArrayList<Integer> partidesPerdudes = new ArrayList<Integer>();
	static Scanner src = new Scanner(System.in);

	public static String DefinirJugador() {
		// TODO Auto-generated method stub
		String jug;
		System.out.print("Nom del jugador:");
		jug = src.nextLine();
		if (!noms.contains(jug)) {
			noms.add(jug);
			partidesPerdudes.add(0);
			partidesGuanyades.add(0);
		}
		return jug;
	}

	public static void ActualitzarResultat(String jugador, boolean guanya) {
		int pos = noms.indexOf(jugador);
		if (pos != -1) {
			if (guanya == true)
				partidesGuanyades.set(pos, partidesGuanyades.get(pos + 1));
			else
				partidesPerdudes.set(pos, partidesPerdudes.get(pos + 1));
		}
	}

	public static void VeureJugador(String jugador) {
		// TODO Auto-generated method stub
		int pos = noms.indexOf(jugador);
		if (pos != -1) {
			System.out.println("El jugador " + jugador + " ha guanyat " + partidesGuanyades.get(pos) + " i ha perdut "
					+ partidesPerdudes.get(pos));
		}
	}

	public static void Inicialitzar() {
		// TODO Auto-generated method stub
		noms.clear();
		partidesGuanyades.clear();
		partidesPerdudes.clear();
	}

	public static void MostrarEstadistica() {
		// TODO Auto-generated method stub
		if (noms.isEmpty()) {
			System.out.println("No hi ha jugadors");
		} else {
			System.out.println("Estadístiques de les partides:");
			System.out.println("Nom jugador     Guanyades     Perdudes     ");
			int pos = 0;
			for (pos = 0; pos < noms.size(); pos++) {
				System.out.println(noms.get(pos) + "                " + partidesGuanyades.get(pos) + "             "
						+ partidesPerdudes.get(pos));
			}
		}
	}
}

package buscamines;

public class Help {

	public static void Ajuda() {
		// TODO Auto-generated method stub
		System.out.println("****  Instruccions joc Buscamines  ******");
		System.out.println("----------------------------------------------");
		System.out.println(
				"El joc consisteix a netejar totes les caselles d'una pantalla que no amaguin una mina. Algunes caselles tenen un número. Aquest número indica les mines que sumen totes les caselles circumdants. Així, si una casella té el número 3 vol dir que de les vuit caselles que l'envolten (excepte si es troba a una vora o una cantonada) n'hi ha 3 amb mines i 5 sense. Si es descobreix una casella sense número ens indica que cap de les caselles veïnes té mina i aquestes es descobreixen automàticament.\r\n"
						+ "\r\n" + "Si es descobreix una casella amb mina es perd la partida.\r\n" + "\r\n"
						+ "Es pot posar una  marca a les caselles en què el jugador pensa que hi ha mina per tal d'ajudar-nos a descobrir les que s'hi troben a prop.");

		System.out.println("-----------------------------------------------");

	}

}
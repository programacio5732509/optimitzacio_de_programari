package buscamines;

import java.util.ArrayList;
import java.util.Scanner;

public class Jugador {

	private static final String MAX = null;
	static Scanner src = new Scanner(System.in);
	static ArrayList<String> nomJugadors = new ArrayList<String>();
	static ArrayList<Integer> partidasGanadas = new ArrayList<Integer>();
	static ArrayList<Integer> partidasPerdidas = new ArrayList<Integer>();

	public static String DefinirJugador() {
		// TODO Auto-generated method stub
		String jug;
		System.out.print("Nom del jugador:");
		jug = src.nextLine();
		if (!nomJugadors.contains(jug)) {
			nomJugadors.add(jug);
			partidasPerdidas.add(0);
			partidasGanadas.add(0);
		}
		return jug;
	}

	public static void ActualitzarResultat(String jugador, boolean guanya) {
		int pos = nomJugadors.indexOf(jugador);
		if (pos != -1) {
			if (guanya == true)
				partidasGanadas.set(pos, partidasGanadas.get(pos + 1));
			else
				partidasPerdidas.set(pos, partidasPerdidas.get(pos + 1));
		}
	}

	public static void VeureJugador(String jugador) {
		// TODO Auto-generated method stub
		int pos = nomJugadors.indexOf(jugador);
		if (pos != -1) {
			System.out.println("El jugador " + jugador + " ha guanyat " + partidasGanadas.get(pos) + " i ha perdut "
					+ partidasPerdidas.get(pos));
		}
	}

	public static void Inicialitzar() {
		// TODO Auto-generated method stub
		nomJugadors.clear();
		partidasGanadas.clear();
		partidasPerdidas.clear();
	}

	public static void MostrarEstadistica() {
		// TODO Auto-generated method stub
		if (nomJugadors.isEmpty()) {
			System.out.println("No hi ha jugadors");
		} else {
			System.out.println("Estadístiques de les partides:");
			System.out.println("Nom jugador     Guanyades     Perdudes     ");
			int pos = 0;
			for (pos = 0; pos < nomJugadors.size(); pos++) {
				System.out.println(nomJugadors.get(pos) + "                " + partidasGanadas.get(pos)
						+ "             " + partidasPerdidas.get(pos));
			}
		}
	}
}
package buscamines;

import java.util.Random;
import java.util.Scanner;

public class Jugar {
	static Scanner src = new Scanner(System.in);

	public static int[][] TablaVisible = new int[10][10];
	public static int[][] TablaBomba = new int[10][10];

	public static void TablaVisible() {
		System.out.print("\t ");
		for (int i = 0; i < 10; i++) {
			System.out.print(" " + i + "  ");
		}
		System.out.print("\n");
		for (int i = 0; i < 10; i++) {
			System.out.print(i + "\t| ");
			for (int j = 0; j < 10; j++) {
				if (TablaVisible[i][j] == 0) {
					System.out.print("?");
				} else if (TablaVisible[i][j] == 50) {
					System.out.print(" ");
				} else {
					System.out.print(TablaVisible[i][j]);
				}
				System.out.print(" | ");
			}
			System.out.print("\n");
		}
	}

	public static void TablaOculta() {
		System.out.print("\t ");
		for (int i = 0; i < 10; i++) {
			System.out.print(" " + i + "  ");
		}
		System.out.print("\n");
		for (int i = 0; i < 10; i++) {
			System.out.print(i + "\t| ");
			for (int j = 0; j < 10; j++) {
				if (TablaBomba[i][j] == 0) {
					System.out.print(" ");
				} else if (TablaBomba[i][j] == 100) {
					System.out.print("X");
				} else {
					System.out.print(TablaBomba[i][j]);
				}
				System.out.print(" | ");
			}
			System.out.print("\n");
		}
	}

	public static void Imprimir(int i, int j) {
		TablaVisible[i][j] = 50;
		if (i != 0) {
			TablaVisible[i - 1][j] = TablaBomba[i - 1][j];
			if (TablaVisible[i - 1][j] == 0)
				TablaVisible[i - 1][j] = 50;
			if (j != 0) {
				TablaVisible[i - 1][j - 1] = TablaBomba[i - 1][j - 1];
				if (TablaVisible[i - 1][j - 1] == 0)
					TablaVisible[i - 1][j - 1] = 50;

			}
		}
		if (i != 9) {
			TablaVisible[i + 1][j] = TablaBomba[i + 1][j];
			if (TablaVisible[i + 1][j] == 0)
				TablaVisible[i + 1][j] = 50;
			if (j != 9) {
				TablaVisible[i + 1][j + 1] = TablaBomba[i + 1][j + 1];
				if (TablaVisible[i + 1][j + 1] == 0)
					TablaVisible[i + 1][j + 1] = 50;
			}
		}
		if (j != 0) {
			TablaVisible[i][j - 1] = TablaBomba[i][j - 1];
			if (TablaVisible[i][j - 1] == 0)
				TablaVisible[i][j - 1] = 50;
			if (i != 9) {
				TablaVisible[i + 1][j - 1] = TablaBomba[i + 1][j - 1];
				if (TablaVisible[i + 1][j - 1] == 0)
					TablaVisible[i + 1][j - 1] = 50;
			}
		}
		if (j != 9) {
			TablaVisible[i][j + 1] = TablaBomba[i][j + 1];
			if (TablaVisible[i][j + 1] == 0)
				TablaVisible[i][j + 1] = 50;
			if (i != 0) {
				TablaVisible[i - 1][j + 1] = TablaBomba[i - 1][j + 1];
				if (TablaVisible[i - 1][j + 1] == 0)
					TablaVisible[i - 1][j + 1] = 50;
			}
		}
	}

	public static void TableroCompleto(int i, int j) {
		Random random = new Random();
		int x = random.nextInt() % 4;

		TablaVisible[i][j] = TablaBomba[i][j];

		if (x == 0) {
			if (i != 0) {
				if (TablaBomba[i - 1][j] != 100) {
					TablaVisible[i - 1][j] = TablaBomba[i - 1][j];
					if (TablaVisible[i - 1][j] == 0)
						TablaVisible[i - 1][j] = 50;
				}
			}
			if (j != 0) {
				if (TablaBomba[i][j - 1] != 100) {
					TablaVisible[i][j - 1] = TablaBomba[i][j - 1];
					if (TablaVisible[i][j - 1] == 0)
						TablaVisible[i][j - 1] = 50;
				}

			}
			if (i != 0 && j != 0) {
				if (TablaBomba[i - 1][j - 1] != 100) {
					TablaVisible[i - 1][j - 1] = TablaBomba[i - 1][j - 1];
					if (TablaVisible[i - 1][j - 1] == 0)
						TablaVisible[i - 1][j - 1] = 50;
				}

			}
		} else if (x == 1) {
			if (i != 0) {
				if (TablaBomba[i - 1][j] != 100) {
					TablaVisible[i - 1][j] = TablaBomba[i - 1][j];
					if (TablaVisible[i - 1][j] == 0)
						TablaVisible[i - 1][j] = 50;
				}
			}
			if (j != 9) {
				if (TablaBomba[i][j + 1] != 100) {
					TablaVisible[i][j + 1] = TablaBomba[i][j + 1];
					if (TablaVisible[i][j + 1] == 0)
						TablaVisible[i][j + 1] = 50;
				}

			}
			if (i != 0 && j != 9) {
				if (TablaBomba[i - 1][j + 1] != 100) {
					TablaVisible[i - 1][j + 1] = TablaBomba[i - 1][j + 1];
					if (TablaVisible[i - 1][j + 1] == 0)
						TablaVisible[i - 1][j + 1] = 50;
				}
			}
		} else if (x == 2) {
			if (i != 9) {
				if (TablaBomba[i + 1][j] != 100) {
					TablaVisible[i + 1][j] = TablaBomba[i + 1][j];
					if (TablaVisible[i + 1][j] == 0)
						TablaVisible[i + 1][j] = 50;
				}
			}
			if (j != 9) {
				if (TablaBomba[i][j + 1] != 100) {
					TablaVisible[i][j + 1] = TablaBomba[i][j + 1];
					if (TablaVisible[i][j + 1] == 0)
						TablaVisible[i][j + 1] = 50;
				}

			}
			if (i != 9 && j != 9) {
				if (TablaBomba[i + 1][j + 1] != 100) {
					TablaVisible[i + 1][j + 1] = TablaBomba[i + 1][j + 1];
					if (TablaVisible[i + 1][j + 1] == 0)
						TablaVisible[i + 1][j + 1] = 50;
				}
			}
		} else {
			if (i != 9) {
				if (TablaBomba[i + 1][j] != 100) {
					TablaVisible[i + 1][j] = TablaBomba[i + 1][j];
					if (TablaVisible[i + 1][j] == 0)
						TablaVisible[i + 1][j] = 50;
				}
			}
			if (j != 0) {
				if (TablaBomba[i][j - 1] != 100) {
					TablaVisible[i][j - 1] = TablaBomba[i][j - 1];
					if (TablaVisible[i][j - 1] == 0)
						TablaVisible[i][j - 1] = 50;
				}

			}
			if (i != 9 && j != 0) {
				if (TablaBomba[i + 1][j - 1] != 100) {
					TablaVisible[i + 1][j - 1] = TablaBomba[i + 1][j - 1];
					if (TablaVisible[i + 1][j - 1] == 0)
						TablaVisible[i + 1][j - 1] = 50;
				}
			}
		}
	}

	public void BombaOculta() {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				int cnt = 0;
				if (TablaBomba[i][j] != 100) {

					if (i != 0) {
						if (TablaBomba[i - 1][j] == 100)
							cnt++;
						if (j != 0) {
							if (TablaBomba[i - 1][j - 1] == 100)
								cnt++;
						}

					}
					if (i != 9) {
						if (TablaBomba[i + 1][j] == 100)
							cnt++;
						if (j != 9) {
							if (TablaBomba[i + 1][j + 1] == 100)
								cnt++;
						}
					}
					if (j != 0) {
						if (TablaBomba[i][j - 1] == 100)
							cnt++;
						if (i != 9) {
							if (TablaBomba[i + 1][j - 1] == 100)
								cnt++;
						}
					}
					if (j != 9) {
						if (TablaBomba[i][j + 1] == 100)
							cnt++;
						if (i != 0) {
							if (TablaBomba[i - 1][j + 1] == 100)
								cnt++;
						}
					}

					TablaBomba[i][j] = cnt;
				}
			}
		}

		// displayHidden();
	}

	public void Actualizar(int diff) {
		int var = 0;
		while (var != 10) {
			Random random = new Random();
			int i = random.nextInt(10);
			int j = random.nextInt(10);
			// System.out.println("i: " + i + " j: " + j);
			TablaBomba[i][j] = 100;
			var++;
		}
		BombaOculta();
	}

	public boolean Ganador() {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if (TablaVisible[i][j] == 0) {
					if (TablaBomba[i][j] != 100) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public Jugar() {
		
		boolean espacio = true;
		while (espacio) {
			TablaVisible();
			espacio = JugarPartida(null);
			if (Ganador()) {
				TablaOculta();
				System.out.println("\n================Has ganado================");
			}
		}
	}

	public static boolean JugarPartida(String jugador) {
		// TODO Auto-generated method stub
		boolean guanya = false;
		TablaVisible();
		System.out.print("\nNumero de fila: ");
		int i = src.nextInt();
		System.out.print(" Numero de columna: ");
		int j = src.nextInt();
		if (i < 0 || i > 9 || j < 0 || j > 9 || TablaVisible[i][j] != 0) {
			System.out.print("\nPon un numero del 0 al 9");
		}
		if (TablaBomba[i][j] == 100) {
			TablaOculta();
			System.out.print("Has pisado una mina.\n============GAME OVER============");
		} else if (TablaBomba[i][j] == 0) {
			Imprimir(i, j);
		} else {
			TableroCompleto(i, j);
		}
		return guanya;
	}
}

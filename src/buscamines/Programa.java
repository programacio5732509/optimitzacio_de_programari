package buscamines;

import java.util.Scanner;
import buscamines.Help;
import buscamines.Jugador;
import buscamines.Jugar;

public class Programa {

	static Scanner src = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int opcio;
		String jugador = "";
		boolean definido = false;

		do {
			opcio = menu();
			switch (opcio) {
			case 1:
				Help.Ajuda();
				break;
			case 2:
				jugador = Jugador.DefinirJugador();
				definido = true;
				break;
			case 3:
				if (definido) {
					Jugar.JugarPartida(jugador);
					definido = false;
				} else
					System.out.println("Error, cal definir primer el jugador");
				break;
			case 4:
					Jugador.VeureJugador(jugador);
					System.out.println("Error, cal definir primer el jugador");
				break;
			case 5:
					Jugador.Inicialitzar();
				break;
			case 6:
				
					Jugador.MostrarEstadistica();
				break;
			case 0:
				System.out.println("GG\n\nBona partida!!!");
				break;
			default:
				System.out.println("Error, opcio incorrecta. Torna-ho a provar..");
			}
		} while (opcio != '0');
		System.out.print("\n‎ 😃 !!!");
	}

	public static int menu() {
		// TODO Auto-generated method stub
		int op = 0;
		boolean valid = false;
		System.out.println("1.- Mostrar Ajuda");
		System.out.println("2.- Nom del jugador");
		System.out.println("3.- Jugar Partida");
		System.out.println("4.- Veure Jugador");
		System.out.println("5.- Inicializar partida");
		System.out.println("6.- Veure Rankings");
		System.out.println("0.- Sortir");
		System.out.println("\nEscull una opcio: ");
		do {
			try {
				op = src.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.println("Error, has de posar un numero enter");
				src.nextLine();
			}
		} while (!valid);
		return op;
	}
}

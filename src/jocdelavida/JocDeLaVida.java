package jocdelavida;

import java.util.Random;
import java.util.Scanner;

/**
 * Aquesta classe defineix el metode per jugar al Joc de la Vida.
 * 
 * @author Abraham Lucena Cuenda
 *
 */
public class JocDeLaVida {
	static Scanner sc = new Scanner(System.in);
	static final int MORT = 0;
	static final int VIDA = 1;

	/**
	 * @param args Defineix els metodes emprats al menu i el seu funcionament.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] tauler = null;
		int op;

		do {
			op = menu();
			switch (op) {
			case 1:
				tauler = inicialitzarTauler();
				break;
			case 2:
				visualitzarTauler(tauler);
				break;
			case 3:
				iterar(tauler);
				break;
			default:
			}
		} while (op != 0);
	}

	/**
	 * 
	 * @return Retorna al metode omplirtauler.
	 */
	public static int[][] inicialitzarTauler() {
		// demanar dimensions tauler
		int f = obtenirDimensio(0);
		int c = obtenirDimensio(1);

		int[][] tauler = new int[f][c];

		// inicizalització tauler
		for (int i = 0; i < tauler.length; i++)
			for (int j = 0; j < tauler[0].length; j++)
				tauler[i][j] = MORT;

		int vives = Vives(f * c);

		omplirTauler(tauler, vives);
		return tauler;
	}

	/**
	 * 
	 * @param tauler Visualitza la matriu de la taula
	 */
	public static void visualitzarTauler(int[][] tauler) {
		if (tauler == null)
			return;
		for (int i = 0; i < tauler.length; i++) {
			for (int j = 0; j < tauler[0].length; j++)
				System.out.print(tauler[i][j] + " ");
			System.out.println();
		}
	}

	/**
	 * 
	 * @param taula1 matriu per calcular la taula actual
	 * @param taula2 matriu per calcular la taula nova
	 */

	public static void calcular(int[][] taula1, int[][] taula2) {
		for (int i = 0; i < taula1.length; i++)
			for (int j = 0; j < taula2[0].length; j++)
				taula2[i][j] = novaPosicio(taula1, i, j);
	}

	/**
	 * 
	 * @param t matriu taula
	 * @param f files de la matriu
	 * @param c columnes de la matriu
	 * @return retorna a la funcio int VIDA o MORT
	 */
	public static int novaPosicio(int[][] t, int f, int c) {
		int costat = 0;
		if (f - 1 >= 0 && c - 1 >= 0)
			costat += t[f - 1][c - 1];
		if (f - 1 >= 0)
			costat += t[f - 1][c];
		if (f - 1 >= 0 && c + 1 < t[0].length)
			costat += t[f - 1][c + 1];
		if (c - 1 >= 0)
			costat += t[f][c - 1];
		if (c + 1 < t[0].length)
			costat += t[f][c + 1];
		if (f + 1 < t.length && c - 1 >= 0)
			costat += t[f + 1][c - 1];
		if (f + 1 < t.length)
			costat += t[f + 1][c];
		if (f + 1 < t.length && c + 1 < t[0].length)
			costat += t[f + 1][c + 1];

		if (t[f][c] == MORT && costat == 3)
			return VIDA;
		if (t[f][c] == VIDA)
			if (costat == 2 || costat == 3)
				return VIDA;
			else
				return MORT;
		return 0;
	}

	/**
	 * 
	 * @param taula1 Defineix la llargada de la matriuActual
	 * @param taula2 Defineix la llargada de la matriuNova
	 */
	public static void copiar(int[][] taula1, int[][] taula2) {
		for (int i = 0; i < taula2.length; i++)
			for (int j = 0; j < taula2[0].length; j++)
				taula1[i][j] = taula2[i][j];
	}

	/**
	 * 
	 * @param tauler Defineix la matriu tauler
	 * @param vives  Defineix les opcions VIVES.
	 */
	public static void omplirTauler(int[][] tauler, int vives) {
		int num = 0;
		int files = tauler.length;
		int columnes = tauler[0].length;
		Random r = new Random(files * columnes);

		while (num < vives) {
			int f = r.nextInt(files);
			int c = r.nextInt(columnes);

			if (tauler[f][c] == MORT) {
				tauler[f][c] = VIDA;
				num++;
			}
		}
	}

	/**
	 * 
	 * @param tauler matriu per definir el tauler.
	 */

	public static void iterar(int tauler[][]) {
		visualitzarTauler(tauler);
		int iteracions = sc.nextInt();
		int[][] taulerNou = new int[tauler.length][tauler[0].length];

		for (int i = 0; i < iteracions; i++) {
			calcular(tauler, taulerNou);
			System.out.println("\n-----------------");
			System.out.println("----  " + i + "----");
			System.out.println("-----------------\n");

			visualitzarTauler(taulerNou);
			copiar(tauler, taulerNou);
		}
	}

	/**
	 * 
	 * @param caselles-Numero de caselles random
	 * @return Retorna per saber el random.
	 */
	public static int Vives(int caselles) {
		Random r = new Random(caselles);
		int random;
		do {
			random = r.nextInt();
		} while ((random < caselles / 4) || (random > caselles / 2));
		return random;
	}

	/**
	 * 
	 * @param x Parametre de la funcio.
	 * @return Retorna al numero que hem posat pel teclat.
	 */
	public static int obtenirDimensio(int x) {
		if (x == 0)
			System.out.print("Quantes files vols?: ");
		else
			System.out.print("Quantes columnes vols?: ");

		return sc.nextInt();
	}

	/**
	 * 
	 * @return Ens defineix el menu i on retorna.
	 */
	public static int menu() {
		System.out.println("1.- Inicialitzar");
		System.out.println("2.- Visualitzar");
		System.out.println("3.- Iterar");
		System.out.println("0.- Sortir");

		return sc.nextInt();
	}

}
